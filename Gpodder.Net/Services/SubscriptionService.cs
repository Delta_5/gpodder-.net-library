﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Threading.Tasks;
using GpodderLib.Dto;
using GpodderLib.Services.Base;

namespace GpodderLib.Services
{
    public class SubscriptionService : SecuredRemoteServiceBase
    {
        private const string ApiUploadUri = "/subscriptions/{username}/{device_id}.json";
        private const string ApiGetUri = "/subscriptions/{username}/{device_id}.json";
        private const string ApiGetAllUri = "/subscriptions/{username}.json";
        private const string ApiUploadChangesUri = "/api/2/subscriptions/{username}/{device_id}.json";
        //private const string ApiGetChangesUri = "/api/2/subscriptions/{username}/{device_id}.json?since={timestamp}";

        public SubscriptionService(Configuration configuration, ConfigurationService configurationService, AuthenticationService authenticationService)
            : base(configuration, configurationService, authenticationService)
        {
        }

        public async Task<List<string>> QueryPodcastsForDevice(string deviceId)
        {
            var configData = await ConfigurationService.GetClientConfig();
            var uri = new Uri(configData.ApiConfig.BaseUrl, ApiGetUri
                .Replace("{device_id}", deviceId)
                .Replace("{username}", Configuration.Username)
                );

            return await Query<List<string>>(uri);
        }

        public async Task<List<Podcast>> QueryAllPodcasts()
        {
            var configData = await ConfigurationService.GetClientConfig();
            var uri = new Uri(configData.ApiConfig.BaseUrl, ApiGetAllUri
                .Replace("{username}", Configuration.Username)
                );

            return await Query<List<Podcast>>(uri);
        }
    }
}
